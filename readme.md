GWSHoloIntegration
---

An Android Project Library that integrates the 3d party libraries together that 
allow 98% of the Android 4.1 and soon the Android 4.2 and Android 5.x look and 
feel replciated in your android mobile applicaion across multiple android OS 
versions from Android 2.x to android 4.x.

# License

[Apache 2.0 License]()

# Integration

3rd party projects are included with their own package naming intact. Modifications 
are made to allow everything to work together. For example, all the included 3rd party 
libs when using AsyncTask call my modified one as I backported the android 4.x one 
to provide some more finer control to AsyncTask execution.

The purpose of the integration is that all calls you would normally do in targeting 
your android application to android 4.x version should be the same exact calls you 
use in targeting the saem android application for android 2.x and android 3.x and yet 
get the EXACT SAME LOOK AND FEEL AS ANDROID 4.x.

# Features

## StrictMode

StrictMode ported back to android 2.1.

## DiskLruCache

## HttpResponseCache

## TwoLevelCache

## LruSoftCache

## Modified AsyncTask

Re-enabled features to execute either as serial or parallel and compatiable 
all the way back to Android 2.1.

## ActionBarSherlock

## NineOldAndroids

## HoloEverywhere

## Seismic


## ScaleGestureDetector and RealScaleGestureDetector from GraphView

## ICS Property

## NotificationCompat2

## ActivityCompat2

## AppMsg(Crouton)

## CollapisbleSearch

## GridFragment

## GridLayout

## ViewPagerIndicator

## SwipeToDissmissNOA

## PulltoRefresh

## Dashboard Library

## Animation backport

## EventBus



