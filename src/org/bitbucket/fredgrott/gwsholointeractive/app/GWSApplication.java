package org.bitbucket.fredgrott.gwsholointeractive.app;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;

import com.WazaBe.HoloEverywhere.app.Application;


/**
 * GWSApplication, is a helper class to enforce android 
 * application development best practices. It extends 
 * HoloeEverywhere's Application class so that we get 
 * a full Android 4.1 UI feel and behavior on OS versions 
 * 3.x and 2.x.
 * 
 * Concepts and code borrowed from several places including
 * GreenDroid.
 * 
 * @author fredgrott
 *
 */
public class GWSApplication extends Application {
	
	private static final int CORE_POOL_SIZE = 5;
	
	private ExecutorService mExecutorService;
	
	// need to define a minimum set of caches, 
	// possibly httpresponsecache, image cache
	// and app two-level cache
	
    private File httpCacheDir;
    private long httpCacheSize;
	
	private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {
            return new Thread(r, "GWSHoloIntegration thread #" + mCount.getAndIncrement());
        }
    };

	public GWSApplication() {
		// TODO Auto-generated constructor stub
		
	}
	
	/**
     * Return an ExecutorService (global to the entire application) that may be
     * used by clients when running long tasks in the background.
     * 
     * @return An ExecutorService to used when processing long running tasks
     */
    public ExecutorService getExecutor() {
        if (mExecutorService == null) {
            mExecutorService = Executors.newFixedThreadPool(CORE_POOL_SIZE, sThreadFactory);
        }
        return mExecutorService;
    }
    
    /**
     * Return the class of the home Activity. The home Activity is the main
     * entrance point of your application. This is usually where the
     * dashboard/general menu is displayed.
     * 
     * @return The Class of the home Activity
     */
    public Class<?> getHomeActivityClass() {
        return null;
    }

    /**
     * Each application may have an "application intent" which will be used when
     * the user clicked on the application button.
     * 
     * @return The main application Intent (may be null if you don't want to use
     *         the main application Intent feature)
     */
    public Intent getMainApplicationIntent() {
        return null;
    }
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}
	
	@Override
	public Context getApplicationContext() {
		// TODO Auto-generated method stub
		return super.getApplicationContext();
	}
	
	@Override
	public ApplicationInfo getApplicationInfo() {
		// TODO Auto-generated method stub
		return super.getApplicationInfo();
	}
	
	@Override
	public void onTerminate() {
		// TODO Auto-generated method stub
		// Always unregister when an object no longer should be on the bus.
	    
		super.onTerminate();
	}
	
	
	@Override
	public void onLowMemory() {
		// TODO Auto-generated method stub
		
		super.onLowMemory();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	public void onTrimMemory(int level) {
		// TODO Auto-generated method stub
		super.onTrimMemory(level);
	}
	
	
	@Override
	public File getCacheDir() {
		// TODO Auto-generated method stub
		return super.getCacheDir();
	}
	
	
	
}
