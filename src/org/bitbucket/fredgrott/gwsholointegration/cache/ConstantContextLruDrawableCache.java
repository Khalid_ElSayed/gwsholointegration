package org.bitbucket.fredgrott.gwsholointegration.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * The Class ConstantContextLruDrawableCache.
 */
public class ConstantContextLruDrawableCache {

	/** The cache. */
	ConstantContextLruCache<String, Drawable> cache;
	
	/**
	 * Instantiates a new constant context lru drawable cache.
	 *
	 * @param maxSize the max size
	 * @param context the context
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ConstantContextLruDrawableCache(int maxSize, Context context) {
		cache = new ConstantContextLruCache(maxSize, context);
	}
	
	/**
	 * Gets the drawable.
	 *
	 * @param string the string
	 * @return the drawable
	 */
	@SuppressWarnings("unchecked")
	public Drawable getDrawable(String string){
		return (Drawable) cache.get(string);
	}
	
	/**
	 * Put drawable.
	 *
	 * @param string the string
	 * @param bitmap the bitmap
	 */
	@SuppressWarnings("unchecked")
	public void putDrawable(String string, Bitmap bitmap){
		cache.put(string,  bitmap);
	}
	
	/**
	 * Clean.
	 */
	public void clean(){
		cache.evictAll();
	}
}