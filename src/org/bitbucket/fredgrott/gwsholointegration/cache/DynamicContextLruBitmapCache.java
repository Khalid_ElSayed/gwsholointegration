package org.bitbucket.fredgrott.gwsholointegration.cache;

import android.graphics.Bitmap;
import android.util.LruCache;


/**
 * The Class DynamicContextLruBitmapCache.
 */
public class DynamicContextLruBitmapCache {
	
	/** The cache. */
	LruCache<ContextAndKey<String>, Bitmap> cache;

	/**
	 * Instantiates a new dynamic context lru bitmap cache.
	 *
	 * @param maxSize the max size
	 */
	public DynamicContextLruBitmapCache(int maxSize) {
		cache = new LruCache<ContextAndKey<String>, Bitmap>(maxSize);
	}
	
	/**
	 * Gets the drawable.
	 *
	 * @param string the string
	 * @return the drawable
	 */
	public Bitmap getDrawable(ContextAndKey<String> string){
		return cache.get(string);
	}
	
	/**
	 * Put drawable.
	 *
	 * @param string the string
	 * @param bitmap the bitmap
	 */
	public void putDrawable(ContextAndKey<String> string, Bitmap bitmap){
		cache.put(string, bitmap);
		
	}
	
	
	/**
	 * Clean.
	 */
	public void clean(){
		cache.evictAll();
	}
}