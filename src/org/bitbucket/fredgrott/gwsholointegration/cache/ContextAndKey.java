package org.bitbucket.fredgrott.gwsholointegration.cache;

import android.content.Context;

public class ContextAndKey<K> {
	@SuppressWarnings("unused")
	private final Context context=null;
	  @SuppressWarnings("unused")
	private final K key=null;
	  public ContextAndKey(Context context, K key) {
	    
	  }
	 /**
	  * must be over-ridden
	  */
	 public boolean equals(Object other) {
		return false;
	   
	  }
	 /**
	  * must be over-ridden
	  */
	  public int hashCode() {
		return 0;
	  
	  }
	  
}