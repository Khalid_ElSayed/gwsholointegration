package org.bitbucket.fredgrott.gwsholointegration.os;

import android.content.Context;

/**
 * * Borrowed from Kaeppler's Ignition Library
 * 
 * Default implementation of the delegate handler interface with all callbacks methods defined to
 * have empty bodies. Subclass this if you do not need to implement all methods of
 * 
 * @author fredgrott
 *
 * @param <ContextT>
 * @param <ProgressT>
 * @param <ReturnT>
 */
public class AsyncTaskDefaultHandler<ContextT extends Context, ProgressT, ReturnT>
implements IAsyncTaskHandler<ContextT, ProgressT, ReturnT> {

    /** The context. */
    private ContextT context;

    /**
     * Instantiates a new gWS async task default handler.
     *
     * @param context the context
     */
    public AsyncTaskDefaultHandler(ContextT context) {
        this.context = context;
    }

    /**
     * Gets the context.
     *
     * @return the context
     * @see org.bitbucket.fredgrott.gwsasynctask.IAsyncTaskHandler#getContext()
     */
    @Override
    public final ContextT getContext() {
        return context;
    }

    /**
     * Sets the context.
     *
     * @param context the new context
     * @see org.bitbucket.fredgrott.gwsasynchtask.IAsyncTaskHandler#setContext(android.content.Context)
     */
    @Override
    public final void setContext(ContextT context) {
        this.context = context;
    }

    /**
     * On task started.
     *
     * @param context the context
     * @return true, if successful
     * @see org.bitbucket.fredgrott.gwsasynchtask.IAsyncTaskHandler#onTaskStarted(android.content.Context)
     */
    @Override
    public boolean onTaskStarted(ContextT context) {
        return false;
    }

    /**
     * On task progress.
     *
     * @param context the context
     * @param progress the progress
     * @return true, if successful
     * @see org.bitbucket.fredgrott.gwsasynctask.IAsyncTaskHandler#onTaskProgress(android.content.Context, ProgressT[])
     */
    @Override
    public boolean onTaskProgress(ContextT context, ProgressT... progress) {
        return false;
    }

    /**
     * On task completed.
     *
     * @param context the context
     * @param result the result
     * @return true, if successful
     * @see org.bitbucket.fredgrott.gwsasynctask.IAsyncTaskHandler#onTaskCompleted(android.content.Context, java.lang.Object)
     */
    @Override
    public boolean onTaskCompleted(ContextT context, ReturnT result) {
        return false;
    }

    /**
     * On task success.
     *
     * @param context the context
     * @param result the result
     * @return true, if successful
     * @see org.bitbucket.fredgrott.gwsasynctask.IAsyncTaskHandler#onTaskSuccess(android.content.Context, java.lang.Object)
     */
    @Override
    public boolean onTaskSuccess(ContextT context, ReturnT result) {
        return false;
    }

    /**
     * On task failed.
     *
     * @param context the context
     * @param error the error
     * @return true, if successful
     * @see org.bitbucket.fredgrott.gwsasynctask.IAsyncTaskHandler#onTaskFailed(android.content.Context, java.lang.Exception)
     */
    @Override
    public boolean onTaskFailed(ContextT context, Exception error) {
        return false;
    }

}