/**
 * Original par of AOSP Copyright Google 2012
 * Apache License 2.0
 */

package org.bitbucket.fredgrott.gwsholointegration.os;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;



import android.os.Handler;
import android.os.Message;
import android.os.Process;

/**
 * Google is making a habit of changing the 
 * Asynctask class for every major android OS release which 
 * makes it damn difficult to get a guaranteed set of behavior 
 * across all platform OS versions.
 * 
 * An example one can produce the result of using same code to 
 * process user stuff with checkboxes and get on the first and 
 * last task submitted rather than the expected behavior of 
 * every task submitted across the android OS versions.
 * 
 * This is meant to be depreciable after awhile when Google 
 * decides to avoid modifying the AsycnTask for every major 
 * Android OS release.
 * 
 * Basically we have made AsyncTask.executeOnExecutor()
 * available on Android OS before api 11. Thus instead  of 
 * using singleThreadPoolExecutor we can now use 
 * AsyncTask.executeonExecutor().
 * 
 * WARNING, Google has stated that they will move towards using 
 * single thread pools which might probably be android 4.2 or 
 * android 5.x which means I will have to rewrite this 
 * code and approach again.
 * 
 * 
 * 
 * 
 * @author fredgrott
 *
 * @param <Params>
 * @param <Progress>
 * @param <Result>
 */
public abstract class ModifiedAsyncTask <Params, Progress, Result> {
    
    /** The Constant LOG_TAG. */
    private static final String LOG_TAG = "AsyncTask";

    /** The Constant CORE_POOL_SIZE. */
    private static final int CORE_POOL_SIZE = 5;
    
    /** The Constant MAXIMUM_POOL_SIZE. */
    private static final int MAXIMUM_POOL_SIZE = 128;
    
    /** The Constant KEEP_ALIVE. */
    private static final int KEEP_ALIVE = 1;

    /** The Constant sThreadFactory. */
    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {
            return new Thread(r, "AsyncTask #" + mCount.getAndIncrement());
        }
    };

    /** The Constant sPoolWorkQueue. */
    private static final BlockingQueue<Runnable> sPoolWorkQueue =
            new LinkedBlockingQueue<Runnable>(10);

    /**
     * An {@link Executor} that can be used to execute tasks in parallel.
     */
    public static final Executor THREAD_POOL_EXECUTOR
            = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE,
                    TimeUnit.SECONDS, sPoolWorkQueue, sThreadFactory);

    /**
     * An {@link Executor} that executes tasks one at a time in serial
     * order.  This serialization is global to a particular process.
     */
//    public static final Executor SERIAL_EXECUTOR = new SerialExecutor();

    private static final int MESSAGE_POST_RESULT = 0x1;
    
    /** The Constant MESSAGE_POST_PROGRESS. */
    private static final int MESSAGE_POST_PROGRESS = 0x2;

    /** The Constant sHandler. */
    private static final InternalHandler sHandler = new InternalHandler();

//    private static volatile Executor sDefaultExecutor = SERIAL_EXECUTOR;
    /** The s default executor. */
private static volatile Executor sDefaultExecutor = THREAD_POOL_EXECUTOR;
    
    /** The m worker. */
    private final WorkerRunnable<Params, Result> mWorker;
    
    /** The m future. */
    private final FutureTask<Result> mFuture;

    /** The m status. */
    private volatile Status mStatus = Status.PENDING;

    /** The m task invoked. */
    private final AtomicBoolean mTaskInvoked = new AtomicBoolean();

//    private static class SerialExecutor implements Executor {
//        final ArrayDeque<Runnable> mTasks = new ArrayDeque<Runnable>();
//        Runnable mActive;
//
//        public synchronized void execute(final Runnable r) {
//            mTasks.offer(new Runnable() {
//                public void run() {
//                    try {
//                        r.run();
//                    } finally {
//                        scheduleNext();
//                    }
//                }
//            });
//            if (mActive == null) {
//                scheduleNext();
//            }
//        }
//
//        protected synchronized void scheduleNext() {
//            if ((mActive = mTasks.poll()) != null) {
//                THREAD_POOL_EXECUTOR.execute(mActive);
//            }
//        }
//    }

    /**
     * Indicates the current status of the task. Each status will be set only once
     * during the lifetime of a task.
     */
    public enum Status {
        /**
         * Indicates that the task has not been executed yet.
         */
        PENDING,
        /**
         * Indicates that the task is running.
         */
        RUNNING,
        /**
         * Indicates that {@link AsyncTask#onPostExecute} has finished.
         */
        FINISHED,
    }

    /**
     * Inits the.
     *
     * @hide Used to force static handler to be created.
     */
    public static void init() {
        sHandler.getLooper();
    }

    /**
     * Sets the default executor.
     *
     * @param exec the new default executor
     * @hide
     */
    public static void setDefaultExecutor(Executor exec) {
        sDefaultExecutor = exec;
    }

    /**
     * Creates a new asynchronous task. This constructor must be invoked on the UI thread.
     */
    public ModifiedAsyncTask() {
        mWorker = new WorkerRunnable<Params, Result>() {
            public Result call() throws Exception {
                mTaskInvoked.set(true);

                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                return postResult(doInBackground(mParams));
            }
        };

        mFuture = new FutureTask<Result>(mWorker) {
            @Override
            protected void done() {
                try {
                    final Result result = get();

                    postResultIfNotInvoked(result);
                } catch (InterruptedException e) {
                    android.util.Log.w(LOG_TAG, e);
                } catch (ExecutionException e) {
                    throw new RuntimeException("An error occured while executing doInBackground()",
                            e.getCause());
                } catch (CancellationException e) {
                    postResultIfNotInvoked(null);
                } catch (Throwable t) {
                    throw new RuntimeException("An error occured while executing "
                            + "doInBackground()", t);
                }
            }
        };
    }

    /**
     * Post result if not invoked.
     *
     * @param result the result
     */
    private void postResultIfNotInvoked(Result result) {
        final boolean wasTaskInvoked = mTaskInvoked.get();
        if (!wasTaskInvoked) {
            postResult(result);
        }
    }

    /**
     * Post result.
     *
     * @param result the result
     * @return the result
     */
    @SuppressWarnings("unchecked")
	private Result postResult(Result result) {
        Message message = sHandler.obtainMessage(MESSAGE_POST_RESULT,
                new GWSAsyncTaskResult<Result>(this, result));
        message.sendToTarget();
        return result;
    }

    /**
     * Returns the current status of this task.
     *
     * @return The current status.
     */
    public final Status getStatus() {
        return mStatus;
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     *
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     *
     * @return A result, defined by the subclass of this task.
     *
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    protected abstract Result doInBackground(Params... params);

    /**
     * Runs on the UI thread before {@link #doInBackground}.
     *
     * @see #onPostExecute
     * @see #doInBackground
     */
    protected void onPreExecute() {
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * 
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param result The result of the operation computed by {@link #doInBackground}.
     *
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object) 
     */
    @SuppressWarnings({})
    protected void onPostExecute(Result result) {
    }

    /**
     * Runs on the UI thread after {@link #publishProgress} is invoked.
     * The specified values are the values passed to {@link #publishProgress}.
     *
     * @param values The values indicating progress.
     *
     * @see #publishProgress
     * @see #doInBackground
     */
    @SuppressWarnings({})
    protected void onProgressUpdate(Progress... values) {
    }

    /**
     * <p>Runs on the UI thread after {@link #cancel(boolean)} is invoked and.
     *
     * @param result The result, if any, computed in
     * {@link #doInBackground(Object[])} has finished.</p>
     * 
     * <p>The default implementation simply invokes {@link #onCancelled()} and
     * ignores the result. If you write your own implementation, do not call
     * <code>super.onCancelled(result)</code>.</p>
     * {@link #doInBackground(Object[])}, can be null
     * @see #cancel(boolean)
     * @see #isCancelled()
     */
    @SuppressWarnings({})
    protected void onCancelled(Result result) {
        onCancelled();
    }    

    /**
     * <p>Applications should preferably override {@link #onCancelled(Object)}.
     * This method is invoked by the default implementation of
     * {@link #onCancelled(Object)}.</p>
     * 
     * <p>Runs on the UI thread after {@link #cancel(boolean)} is invoked and
     * {@link #doInBackground(Object[])} has finished.</p>
     *
     * @see #onCancelled(Object) 
     * @see #cancel(boolean)
     * @see #isCancelled()
     */
    protected void onCancelled() {
    }

    /**
     * Returns <tt>true</tt> if this task was cancelled before it completed
     * normally. If you are calling {@link #cancel(boolean)} on the task,
     * the value returned by this method should be checked periodically from
     *
     * @return <tt>true</tt> if task was cancelled before it completed
     * {@link #doInBackground(Object[])} to end the task as soon as possible.
     * @see #cancel(boolean)
     */
    public final boolean isCancelled() {
        return mFuture.isCancelled();
    }

    /**
     * <p>Attempts to cancel execution of this task.  This attempt will
     * fail if the task has already completed, already been cancelled,
     * or could not be cancelled for some other reason. If successful,
     * and this task has not started when <tt>cancel</tt> is called,
     * this task should never run. If the task has already started,
     * then the <tt>mayInterruptIfRunning</tt> parameter determines
     * whether the thread executing this task should be interrupted in
     * an attempt to stop the task.</p>
     * 
     * <p>Calling this method will result in {@link #onCancelled(Object)} being
     * invoked on the UI thread after {@link #doInBackground(Object[])}
     * returns. Calling this method guarantees that {@link #onPostExecute(Object)}
     * is never invoked. After invoking this method, you should check the
     * value returned by {@link #isCancelled()} periodically from
     *
     * @param mayInterruptIfRunning <tt>true</tt> if the thread executing this
     * task should be interrupted; otherwise, in-progress tasks are allowed
     * to complete.
     * @return <tt>false</tt> if the task could not be cancelled,
     * typically because it has already completed normally;
     * <tt>true</tt> otherwise
     * {@link #doInBackground(Object[])} to finish the task as early as
     * possible.</p>
     * @see #isCancelled()
     * @see #onCancelled(Object)
     */
    public final boolean cancel(boolean mayInterruptIfRunning) {
        return mFuture.cancel(mayInterruptIfRunning);
    }

    /**
     * Waits if necessary for the computation to complete, and then
     * retrieves its result.
     *
     * @return The computed result.
     * @throws InterruptedException If the current thread was interrupted
     * while waiting.
     * @throws ExecutionException If the computation threw an exception.
     */
    public final Result get() throws InterruptedException, ExecutionException {
        return mFuture.get();
    }

    /**
     * Waits if necessary for at most the given time for the computation
     * to complete, and then retrieves its result.
     *
     * @param timeout Time to wait before cancelling the operation.
     * @param unit The time unit for the timeout.
     * @return The computed result.
     * @throws InterruptedException If the current thread was interrupted
     * while waiting.
     * @throws ExecutionException If the computation threw an exception.
     * @throws TimeoutException If the wait timed out.
     */
    public final Result get(long timeout, TimeUnit unit) throws InterruptedException,
            ExecutionException, TimeoutException {
        return mFuture.get(timeout, unit);
    }

    /**
     * Executes the task with the specified parameters. The task returns
     * itself (this) so that the caller can keep a reference to it.
     * 
     * <p>Note: this function schedules the task on a queue for a single background
     * thread or pool of threads depending on the platform version.  When first
     * introduced, AsyncTasks were executed serially on a single background thread.
     * Starting with {@link android.os.Build.VERSION_CODES#DONUT}, this was changed
     * to a pool of threads allowing multiple tasks to operate in parallel.  After
     *
     * @param params The parameters of the task.
     * @return This instance of AsyncTask.
     * {@link android.os.Build.VERSION_CODES#HONEYCOMB}, it is planned to change this
     * back to a single thread to avoid common application errors caused
     * by parallel execution.  If you truly want parallel execution, you can use
     * the {@link #executeOnExecutor} version of this method
     * with {@link #THREAD_POOL_EXECUTOR}; however, see commentary there for warnings on
     * its use.
     * 
     * <p>This method must be invoked on the UI thread.
     * {@link AsyncTask.Status#RUNNING} or {@link AsyncTask.Status#FINISHED}.
     */
    public final ModifiedAsyncTask<Params, Progress, Result> execute(Params... params) {
        return executeOnExecutor(sDefaultExecutor, params);
    }

    /**
     * Executes the task with the specified parameters. The task returns
     * itself (this) so that the caller can keep a reference to it.
     * 
     * <p>This method is typically used with {@link #THREAD_POOL_EXECUTOR} to
     * allow multiple tasks to run in parallel on a pool of threads managed by
     * AsyncTask, however you can also use your own {@link Executor} for custom
     * behavior.
     * 
     * <p><em>Warning:</em> Allowing multiple tasks to run in parallel from
     * a thread pool is generally <em>not</em> what one wants, because the order
     * of their operation is not defined.  For example, if these tasks are used
     * to modify any state in common (such as writing a file due to a button click),
     * there are no guarantees on the order of the modifications.
     * Without careful work it is possible in rare cases for the newer version
     * of the data to be over-written by an older one, leading to obscure data
     * loss and stability issues.  Such changes are best
     * executed in serial; to guarantee such work is serialized regardless of
     * platform version you can use this function with {@link #SERIAL_EXECUTOR}.
     * 
     * <p>This method must be invoked on the UI thread.
     *
     * @param exec The executor to use.  {@link #THREAD_POOL_EXECUTOR} is available as a
     * convenient process-wide thread pool for tasks that are loosely coupled.
     * @param params The parameters of the task.
     * @return This instance of AsyncTask.
     * {@link AsyncTask.Status#RUNNING} or {@link AsyncTask.Status#FINISHED}.
     */
    @SuppressWarnings("incomplete-switch")
	public final ModifiedAsyncTask<Params, Progress, Result> executeOnExecutor(Executor exec,
            Params... params) {
        if (mStatus != Status.PENDING) {
            switch (mStatus) {
                case RUNNING:
                    throw new IllegalStateException("Cannot execute task:"
                            + " the task is already running.");
                case FINISHED:
                    throw new IllegalStateException("Cannot execute task:"
                            + " the task has already been executed "
                            + "(a task can be executed only once)");
            }
        }

        mStatus = Status.RUNNING;

        onPreExecute();

        mWorker.mParams = params;
        exec.execute(mFuture);

        return this;
    }

    /**
     * Convenience version of {@link #execute(Object...)} for use with
     * a simple Runnable object.
     *
     * @param runnable the runnable
     */
    public static void execute(Runnable runnable) {
        sDefaultExecutor.execute(runnable);
    }

    /**
     * This method can be invoked from {@link #doInBackground} to
     * publish updates on the UI thread while the background computation is
     * still running. Each call to this method will trigger the execution of
     *
     * @param values The progress values to update the UI with.
     * {@link #onProgressUpdate} on the UI thread.
     * {@link #onProgressUpdate} will note be called if the task has been
     * canceled.
     * @see #onProgressUpdate
     * @see #doInBackground
     */
    protected final void publishProgress(Progress... values) {
        if (!isCancelled()) {
            sHandler.obtainMessage(MESSAGE_POST_PROGRESS,
                    new GWSAsyncTaskResult<Progress>(this, values)).sendToTarget();
        }
    }

    /**
     * Finish.
     *
     * @param result the result
     */
    private void finish(Result result) {
        if (isCancelled()) {
            onCancelled(result);
        } else {
            onPostExecute(result);
        }
        mStatus = Status.FINISHED;
    }

    /**
     * The Class InternalHandler.
     */
    private static class InternalHandler extends Handler {
        
        /**
         * Handle message.
         *
         * @param msg the msg
         * @see android.os.Handler#handleMessage(android.os.Message)
         */
        @SuppressWarnings({"unchecked", "rawtypes"})
        @Override
        public void handleMessage(Message msg) {
            GWSAsyncTaskResult result = (GWSAsyncTaskResult) msg.obj;
            switch (msg.what) {
                case MESSAGE_POST_RESULT:
                    // There is only one result
                    result.mTask.finish(result.mData[0]);
                    break;
                case MESSAGE_POST_PROGRESS:
                    result.mTask.onProgressUpdate(result.mData);
                    break;
            }
        }
    }

    /**
     * The Class WorkerRunnable.
     *
     * @param <Params> the generic type
     * @param <Result> the generic type
     */
    private static abstract class WorkerRunnable<Params, Result> implements Callable<Result> {
        
        /** The m params. */
        Params[] mParams;
    }

    /**
     * The Class AsyncTaskResult.
     *
     * @param <Data> the generic type
     */
    @SuppressWarnings({})
    private static class GWSAsyncTaskResult<Data> {
        
        /** The m task. */
        @SuppressWarnings("rawtypes")
		final ModifiedAsyncTask mTask;
        
        /** The m data. */
        final Data[] mData;

        /**
         * Instantiates a new async task result.
         *
         * @param task the task
         * @param data the data
         */
        @SuppressWarnings("rawtypes")
		GWSAsyncTaskResult(ModifiedAsyncTask task, Data... data) {
            mTask = task;
            mData = data;
        }
    }
}