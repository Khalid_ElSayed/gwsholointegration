/*
 * Copyright (C) 2011 The Android Open Source Project
 * Modifications Fred Grott Copyright (C) 2012
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package libcore.io;

import java.io.IOException;
import java.net.SocketException;


/**
 * A checked exception thrown when {@link Os} methods fail. This exception contains the native
 * errno value, for comparison against the constants in {@link OsConstants}, should sophisticated
 * callers need to adjust their behavior based on the exact failure.
 */
@SuppressWarnings("serial")
public final class ErrnoException extends Exception {
    
    /** The function name. */
    private final String functionName;
    
    /** The errno. */
    public final int errno;

    /**
     * Instantiates a new errno exception.
     *
     * @param functionName the function name
     * @param errno the errno
     */
    public ErrnoException(String functionName, int errno) {
        this.functionName = functionName;
        this.errno = errno;
    }

    /**
     * Instantiates a new errno exception.
     *
     * @param functionName the function name
     * @param errno the errno
     * @param cause the cause
     */
    public ErrnoException(String functionName, int errno, Throwable cause) {
        super(cause);
        this.functionName = functionName;
        this.errno = errno;
    }

    /**
     * Converts the stashed function name and errno value to a human-readable string.
     * We do this here rather than in the constructor so that callers only pay for
     * this if they need it.
     *
     * @return the message
     */
    @Override public String getMessage() {
        String errnoName = OsConstants.errnoName(errno);
        if (errnoName == null) {
            errnoName = "errno " + errno;
        }
        return functionName + " failed: " + errnoName;
    }

    /**
     * Rethrow as io exception.
     *
     * @return the iO exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public IOException rethrowAsIOException() throws IOException {
        IOException newException = new IOException(getMessage());
        newException.initCause(this);
        throw newException;
    }

    /**
     * Rethrow as socket exception.
     *
     * @return the socket exception
     * @throws SocketException the socket exception
     */
    public SocketException rethrowAsSocketException() throws SocketException {
        throw new com.integralblue.httpresponsecache.compat.java.net.SocketException(getMessage(), this);
    }
}
