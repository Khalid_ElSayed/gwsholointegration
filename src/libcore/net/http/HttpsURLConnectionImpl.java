/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package libcore.net.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.CacheResponse;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SecureCacheResponse;
import java.net.URL;
import java.security.Permission;
import java.security.Principal;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import com.integralblue.httpresponsecache.compat.URLs;


/**
 * The Class HttpsURLConnectionImpl.
 */
final class HttpsURLConnectionImpl extends HttpsURLConnection {

    /** HttpUrlConnectionDelegate allows reuse of HttpURLConnectionImpl. */
    private final HttpUrlConnectionDelegate delegate;

    /**
     * Instantiates a new https url connection impl.
     *
     * @param url the url
     * @param port the port
     */
    protected HttpsURLConnectionImpl(URL url, int port) {
        super(url);
        delegate = new HttpUrlConnectionDelegate(url, port);
    }

    /**
     * Instantiates a new https url connection impl.
     *
     * @param url the url
     * @param port the port
     * @param proxy the proxy
     */
    protected HttpsURLConnectionImpl(URL url, int port, Proxy proxy) {
        super(url);
        delegate = new HttpUrlConnectionDelegate(url, port, proxy);
    }

    /**
     * Check connected.
     */
    private void checkConnected() {
        if (delegate.getSSLSocket() == null) {
            throw new IllegalStateException("Connection has not yet been established");
        }
    }

    /**
     * Gets the http engine.
     *
     * @return the http engine
     */
    HttpEngine getHttpEngine() {
        return delegate.getHttpEngine();
    }

    /**
     * Gets the cipher suite.
     *
     * @return the cipher suite
     * @see javax.net.ssl.HttpsURLConnection#getCipherSuite()
     */
    @Override
    public String getCipherSuite() {
        SecureCacheResponse cacheResponse = delegate.getCacheResponse();
        if (cacheResponse != null) {
            return cacheResponse.getCipherSuite();
        }
        checkConnected();
        return delegate.getSSLSocket().getSession().getCipherSuite();
    }

    /**
     * Gets the local certificates.
     *
     * @return the local certificates
     * @see javax.net.ssl.HttpsURLConnection#getLocalCertificates()
     */
    @Override
    public Certificate[] getLocalCertificates() {
        SecureCacheResponse cacheResponse = delegate.getCacheResponse();
        if (cacheResponse != null) {
            List<Certificate> result = cacheResponse.getLocalCertificateChain();
            return result != null ? result.toArray(new Certificate[result.size()]) : null;
        }
        checkConnected();
        return delegate.getSSLSocket().getSession().getLocalCertificates();
    }

    /**
     * Gets the server certificates.
     *
     * @return the server certificates
     * @throws SSLPeerUnverifiedException the sSL peer unverified exception
     * @see javax.net.ssl.HttpsURLConnection#getServerCertificates()
     */
    @Override
    public Certificate[] getServerCertificates() throws SSLPeerUnverifiedException {
        SecureCacheResponse cacheResponse = delegate.getCacheResponse();
        if (cacheResponse != null) {
            List<Certificate> result = cacheResponse.getServerCertificateChain();
            return result != null ? result.toArray(new Certificate[result.size()]) : null;
        }
        checkConnected();
        return delegate.getSSLSocket().getSession().getPeerCertificates();
    }

    /**
     * Gets the peer principal.
     *
     * @return the peer principal
     * @throws SSLPeerUnverifiedException the sSL peer unverified exception
     * @see javax.net.ssl.HttpsURLConnection#getPeerPrincipal()
     */
    @Override
    public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
        SecureCacheResponse cacheResponse = delegate.getCacheResponse();
        if (cacheResponse != null) {
            return cacheResponse.getPeerPrincipal();
        }
        checkConnected();
        return delegate.getSSLSocket().getSession().getPeerPrincipal();
    }

    /**
     * Gets the local principal.
     *
     * @return the local principal
     * @see javax.net.ssl.HttpsURLConnection#getLocalPrincipal()
     */
    @Override
    public Principal getLocalPrincipal() {
        SecureCacheResponse cacheResponse = delegate.getCacheResponse();
        if (cacheResponse != null) {
            return cacheResponse.getLocalPrincipal();
        }
        checkConnected();
        return delegate.getSSLSocket().getSession().getLocalPrincipal();
    }

    /**
     * Disconnect.
     *
     * @see java.net.HttpURLConnection#disconnect()
     */
    @Override
    public void disconnect() {
        delegate.disconnect();
    }

    /**
     * Gets the error stream.
     *
     * @return the error stream
     * @see java.net.HttpURLConnection#getErrorStream()
     */
    @Override
    public InputStream getErrorStream() {
        return delegate.getErrorStream();
    }

    /**
     * Gets the request method.
     *
     * @return the request method
     * @see java.net.HttpURLConnection#getRequestMethod()
     */
    @Override
    public String getRequestMethod() {
        return delegate.getRequestMethod();
    }

    /**
     * Gets the response code.
     *
     * @return the response code
     * @throws IOException Signals that an I/O exception has occurred.
     * @see java.net.HttpURLConnection#getResponseCode()
     */
    @Override
    public int getResponseCode() throws IOException {
        return delegate.getResponseCode();
    }

    /**
     * Gets the response message.
     *
     * @return the response message
     * @throws IOException Signals that an I/O exception has occurred.
     * @see java.net.HttpURLConnection#getResponseMessage()
     */
    @Override
    public String getResponseMessage() throws IOException {
        return delegate.getResponseMessage();
    }

    /**
     * Sets the request method.
     *
     * @param method the new request method
     * @throws ProtocolException the protocol exception
     * @see java.net.HttpURLConnection#setRequestMethod(java.lang.String)
     */
    @Override
    public void setRequestMethod(String method) throws ProtocolException {
        delegate.setRequestMethod(method);
    }

    /**
     * Using proxy.
     *
     * @return true, if successful
     * @see java.net.HttpURLConnection#usingProxy()
     */
    @Override
    public boolean usingProxy() {
        return delegate.usingProxy();
    }

    /**
     * Gets the instance follow redirects.
     *
     * @return the instance follow redirects
     * @see java.net.HttpURLConnection#getInstanceFollowRedirects()
     */
    @Override
    public boolean getInstanceFollowRedirects() {
        return delegate.getInstanceFollowRedirects();
    }

    /**
     * Sets the instance follow redirects.
     *
     * @param followRedirects the new instance follow redirects
     * @see java.net.HttpURLConnection#setInstanceFollowRedirects(boolean)
     */
    @Override
    public void setInstanceFollowRedirects(boolean followRedirects) {
        delegate.setInstanceFollowRedirects(followRedirects);
    }

    /**
     * Connect.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     * @see java.net.URLConnection#connect()
     */
    @Override
    public void connect() throws IOException {
        connected = true;
        delegate.connect();
    }

    /**
     * Gets the allow user interaction.
     *
     * @return the allow user interaction
     * @see java.net.URLConnection#getAllowUserInteraction()
     */
    @Override
    public boolean getAllowUserInteraction() {
        return delegate.getAllowUserInteraction();
    }

    /**
     * Gets the content.
     *
     * @return the content
     * @throws IOException Signals that an I/O exception has occurred.
     * @see java.net.URLConnection#getContent()
     */
    @Override
    public Object getContent() throws IOException {
        return delegate.getContent();
    }

    /**
     * Gets the content.
     *
     * @param types the types
     * @return the content
     * @throws IOException Signals that an I/O exception has occurred.
     * @see java.net.URLConnection#getContent(java.lang.Class[])
     */
    @SuppressWarnings("rawtypes")
	@Override
    public Object getContent(Class[] types) throws IOException {
        return delegate.getContent(types);
    }

    /**
     * Gets the content encoding.
     *
     * @return the content encoding
     * @see java.net.HttpURLConnection#getContentEncoding()
     */
    @Override
    public String getContentEncoding() {
        return delegate.getContentEncoding();
    }

    /**
     * Gets the content length.
     *
     * @return the content length
     * @see java.net.URLConnection#getContentLength()
     */
    @Override
    public int getContentLength() {
        return delegate.getContentLength();
    }

    /**
     * Gets the content type.
     *
     * @return the content type
     * @see java.net.URLConnection#getContentType()
     */
    @Override
    public String getContentType() {
        return delegate.getContentType();
    }

    /**
     * Gets the date.
     *
     * @return the date
     * @see java.net.URLConnection#getDate()
     */
    @Override
    public long getDate() {
        return delegate.getDate();
    }

    /**
     * Gets the default use caches.
     *
     * @return the default use caches
     * @see java.net.URLConnection#getDefaultUseCaches()
     */
    @Override
    public boolean getDefaultUseCaches() {
        return delegate.getDefaultUseCaches();
    }

    /**
     * Gets the do input.
     *
     * @return the do input
     * @see java.net.URLConnection#getDoInput()
     */
    @Override
    public boolean getDoInput() {
        return delegate.getDoInput();
    }

    /**
     * Gets the do output.
     *
     * @return the do output
     * @see java.net.URLConnection#getDoOutput()
     */
    @Override
    public boolean getDoOutput() {
        return delegate.getDoOutput();
    }

    /**
     * Gets the expiration.
     *
     * @return the expiration
     * @see java.net.URLConnection#getExpiration()
     */
    @Override
    public long getExpiration() {
        return delegate.getExpiration();
    }

    /**
     * Gets the header field.
     *
     * @param pos the pos
     * @return the header field
     * @see java.net.URLConnection#getHeaderField(int)
     */
    @Override
    public String getHeaderField(int pos) {
        return delegate.getHeaderField(pos);
    }

    /**
     * Gets the header fields.
     *
     * @return the header fields
     * @see java.net.URLConnection#getHeaderFields()
     */
    @Override
    public Map<String, List<String>> getHeaderFields() {
        return delegate.getHeaderFields();
    }

    /**
     * Gets the request properties.
     *
     * @return the request properties
     * @see java.net.URLConnection#getRequestProperties()
     */
    @Override
    public Map<String, List<String>> getRequestProperties() {
        return delegate.getRequestProperties();
    }

    /**
     * Adds the request property.
     *
     * @param field the field
     * @param newValue the new value
     * @see java.net.URLConnection#addRequestProperty(java.lang.String, java.lang.String)
     */
    @Override
    public void addRequestProperty(String field, String newValue) {
        delegate.addRequestProperty(field, newValue);
    }

    /**
     * Gets the header field.
     *
     * @param key the key
     * @return the header field
     * @see java.net.URLConnection#getHeaderField(java.lang.String)
     */
    @Override
    public String getHeaderField(String key) {
        return delegate.getHeaderField(key);
    }

    /**
     * Gets the header field date.
     *
     * @param field the field
     * @param defaultValue the default value
     * @return the header field date
     * @see java.net.HttpURLConnection#getHeaderFieldDate(java.lang.String, long)
     */
    @Override
    public long getHeaderFieldDate(String field, long defaultValue) {
        return delegate.getHeaderFieldDate(field, defaultValue);
    }

    /**
     * Gets the header field int.
     *
     * @param field the field
     * @param defaultValue the default value
     * @return the header field int
     * @see java.net.URLConnection#getHeaderFieldInt(java.lang.String, int)
     */
    @Override
    public int getHeaderFieldInt(String field, int defaultValue) {
        return delegate.getHeaderFieldInt(field, defaultValue);
    }

    /**
     * Gets the header field key.
     *
     * @param posn the posn
     * @return the header field key
     * @see java.net.URLConnection#getHeaderFieldKey(int)
     */
    @Override
    public String getHeaderFieldKey(int posn) {
        return delegate.getHeaderFieldKey(posn);
    }

    /**
     * Gets the if modified since.
     *
     * @return the if modified since
     * @see java.net.URLConnection#getIfModifiedSince()
     */
    @Override
    public long getIfModifiedSince() {
        return delegate.getIfModifiedSince();
    }

    /**
     * Gets the input stream.
     *
     * @return the input stream
     * @throws IOException Signals that an I/O exception has occurred.
     * @see java.net.URLConnection#getInputStream()
     */
    @Override
    public InputStream getInputStream() throws IOException {
        return delegate.getInputStream();
    }

    /**
     * Gets the last modified.
     *
     * @return the last modified
     * @see java.net.URLConnection#getLastModified()
     */
    @Override
    public long getLastModified() {
        return delegate.getLastModified();
    }

    /**
     * Gets the output stream.
     *
     * @return the output stream
     * @throws IOException Signals that an I/O exception has occurred.
     * @see java.net.URLConnection#getOutputStream()
     */
    @Override
    public OutputStream getOutputStream() throws IOException {
        return delegate.getOutputStream();
    }

    /**
     * Gets the permission.
     *
     * @return the permission
     * @throws IOException Signals that an I/O exception has occurred.
     * @see java.net.HttpURLConnection#getPermission()
     */
    @Override
    public Permission getPermission() throws IOException {
        return delegate.getPermission();
    }

    /**
     * Gets the request property.
     *
     * @param field the field
     * @return the request property
     * @see java.net.URLConnection#getRequestProperty(java.lang.String)
     */
    @Override
    public String getRequestProperty(String field) {
        return delegate.getRequestProperty(field);
    }

    /**
     * Gets the url.
     *
     * @return the url
     * @see java.net.URLConnection#getURL()
     */
    @Override
    public URL getURL() {
        return delegate.getURL();
    }

    /**
     * Gets the use caches.
     *
     * @return the use caches
     * @see java.net.URLConnection#getUseCaches()
     */
    @Override
    public boolean getUseCaches() {
        return delegate.getUseCaches();
    }

    /**
     * Sets the allow user interaction.
     *
     * @param newValue the new allow user interaction
     * @see java.net.URLConnection#setAllowUserInteraction(boolean)
     */
    @Override
    public void setAllowUserInteraction(boolean newValue) {
        delegate.setAllowUserInteraction(newValue);
    }

    /**
     * Sets the default use caches.
     *
     * @param newValue the new default use caches
     * @see java.net.URLConnection#setDefaultUseCaches(boolean)
     */
    @Override
    public void setDefaultUseCaches(boolean newValue) {
        delegate.setDefaultUseCaches(newValue);
    }

    /**
     * Sets the do input.
     *
     * @param newValue the new do input
     * @see java.net.URLConnection#setDoInput(boolean)
     */
    @Override
    public void setDoInput(boolean newValue) {
        delegate.setDoInput(newValue);
    }

    /**
     * Sets the do output.
     *
     * @param newValue the new do output
     * @see java.net.URLConnection#setDoOutput(boolean)
     */
    @Override
    public void setDoOutput(boolean newValue) {
        delegate.setDoOutput(newValue);
    }

    /**
     * Sets the if modified since.
     *
     * @param newValue the new if modified since
     * @see java.net.URLConnection#setIfModifiedSince(long)
     */
    @Override
    public void setIfModifiedSince(long newValue) {
        delegate.setIfModifiedSince(newValue);
    }

    /**
     * Sets the request property.
     *
     * @param field the field
     * @param newValue the new value
     * @see java.net.URLConnection#setRequestProperty(java.lang.String, java.lang.String)
     */
    @Override
    public void setRequestProperty(String field, String newValue) {
        delegate.setRequestProperty(field, newValue);
    }

    /**
     * Sets the use caches.
     *
     * @param newValue the new use caches
     * @see java.net.URLConnection#setUseCaches(boolean)
     */
    @Override
    public void setUseCaches(boolean newValue) {
        delegate.setUseCaches(newValue);
    }

    /**
     * Sets the connect timeout.
     *
     * @param setConnectTimeout the new connect timeout
     * @see java.net.URLConnection#setConnectTimeout(int)
     */
    @Override
    public void setConnectTimeout(int setConnectTimeout) {
        delegate.setConnectTimeout(setConnectTimeout);
    }

    /**
     * Gets the connect timeout.
     *
     * @return the connect timeout
     * @see java.net.URLConnection#getConnectTimeout()
     */
    @Override
    public int getConnectTimeout() {
        return delegate.getConnectTimeout();
    }

    /**
     * Sets the read timeout.
     *
     * @param timeoutMillis the new read timeout
     * @see java.net.URLConnection#setReadTimeout(int)
     */
    @Override
    public void setReadTimeout(int timeoutMillis) {
        delegate.setReadTimeout(timeoutMillis);
    }

    /**
     * Gets the read timeout.
     *
     * @return the read timeout
     * @see java.net.URLConnection#getReadTimeout()
     */
    @Override
    public int getReadTimeout() {
        return delegate.getReadTimeout();
    }

    /**
     * To string.
     *
     * @return the string
     * @see java.net.URLConnection#toString()
     */
    @Override
    public String toString() {
        return delegate.toString();
    }

    /**
     * Sets the fixed length streaming mode.
     *
     * @param contentLength the new fixed length streaming mode
     * @see java.net.HttpURLConnection#setFixedLengthStreamingMode(int)
     */
    @Override
    public void setFixedLengthStreamingMode(int contentLength) {
        delegate.setFixedLengthStreamingMode(contentLength);
    }

    /**
     * Sets the chunked streaming mode.
     *
     * @param chunkLength the new chunked streaming mode
     * @see java.net.HttpURLConnection#setChunkedStreamingMode(int)
     */
    @Override
    public void setChunkedStreamingMode(int chunkLength) {
        delegate.setChunkedStreamingMode(chunkLength);
    }

    /**
     * The Class HttpUrlConnectionDelegate.
     */
    private final class HttpUrlConnectionDelegate extends HttpURLConnectionImpl {
        
        /**
         * Instantiates a new http url connection delegate.
         *
         * @param url the url
         * @param port the port
         */
        private HttpUrlConnectionDelegate(URL url, int port) {
            super(url, port);
        }

        /**
         * Instantiates a new http url connection delegate.
         *
         * @param url the url
         * @param port the port
         * @param proxy the proxy
         */
        private HttpUrlConnectionDelegate(URL url, int port, Proxy proxy) {
            super(url, port, proxy);
        }

        /**
         * New http engine.
         *
         * @param method the method
         * @param requestHeaders the request headers
         * @param connection the connection
         * @param requestBody the request body
         * @return the http engine
         * @throws IOException Signals that an I/O exception has occurred.
         * @see libcore.net.http.HttpURLConnectionImpl#newHttpEngine(java.lang.String, libcore.net.http.RawHeaders, libcore.net.http.HttpConnection, libcore.net.http.RetryableOutputStream)
         */
        @Override protected HttpEngine newHttpEngine(String method, RawHeaders requestHeaders,
                HttpConnection connection, RetryableOutputStream requestBody) throws IOException {
            return new HttpsEngine(this, method, requestHeaders, connection, requestBody,
                    HttpsURLConnectionImpl.this);
        }

        /**
         * Gets the cache response.
         *
         * @return the cache response
         */
        public SecureCacheResponse getCacheResponse() {
            HttpsEngine engine = (HttpsEngine) httpEngine;
            return engine != null ? (SecureCacheResponse) engine.getCacheResponse() : null;
        }

        /**
         * Gets the sSL socket.
         *
         * @return the sSL socket
         */
        public SSLSocket getSSLSocket() {
            HttpsEngine engine = (HttpsEngine) httpEngine;
            return engine != null ? engine.sslSocket : null;
        }
    }

    /**
     * The Class HttpsEngine.
     */
    private static class HttpsEngine extends HttpEngine {

        /**
         * Local stash of HttpsEngine.connection.sslSocket for answering
         * queries such as getCipherSuite even after
         * httpsEngine.Connection has been recycled. It's presence is also
         * used to tell if the HttpsURLConnection is considered connected,
         * as opposed to the connected field of URLConnection or the a
         * non-null connect in HttpURLConnectionImpl
         */
        private SSLSocket sslSocket;

        /** The enclosing. */
        private final HttpsURLConnectionImpl enclosing;

        /**
         * Instantiates a new https engine.
         *
         * @param policy the HttpURLConnectionImpl with connection configuration
         * @param method the method
         * @param requestHeaders the request headers
         * @param connection the connection
         * @param requestBody the request body
         * @param enclosing the HttpsURLConnection with HTTPS features
         * @throws IOException Signals that an I/O exception has occurred.
         */
        private HttpsEngine(HttpURLConnectionImpl policy, String method, RawHeaders requestHeaders,
                HttpConnection connection, RetryableOutputStream requestBody,
                HttpsURLConnectionImpl enclosing) throws IOException {
            super(policy, method, requestHeaders, connection, requestBody);
            this.sslSocket = connection != null ? connection.getSecureSocketIfConnected() : null;
            this.enclosing = enclosing;
        }

        /**
         * Connect.
         *
         * @throws IOException Signals that an I/O exception has occurred.
         * @see libcore.net.http.HttpEngine#connect()
         */
        @Override protected void connect() throws IOException {
            // first try an SSL connection with compression and
            // various TLS extensions enabled, if it fails (and its
            // not unheard of that it will) fallback to a more
            // barebones connections
            boolean connectionReused;
            try {
                connectionReused = makeSslConnection(true);
            } catch (IOException e) {
                // If the problem was a CertificateException from the X509TrustManager,
                // do not retry, we didn't have an abrupt server initiated exception.
                if (e instanceof SSLHandshakeException
                        && e.getCause() instanceof CertificateException) {
                    throw e;
                }
                release(false);
                connectionReused = makeSslConnection(false);
            }

            if (!connectionReused) {
                sslSocket = connection.verifySecureSocketHostname(enclosing.getHostnameVerifier());
            }
        }

        /**
         * Attempt to make an https connection. Returns true if a
         * connection was reused, false otherwise.
         *
         * @param tlsTolerant If true, assume server can handle common
         * TLS extensions and SSL deflate compression. If false, use
         * an SSL3 only fallback mode without compression.
         * @return true, if successful
         * @throws IOException Signals that an I/O exception has occurred.
         */
        private boolean makeSslConnection(boolean tlsTolerant) throws IOException {
            // make an SSL Tunnel on the first message pair of each SSL + proxy connection
            if (connection == null) {
                connection = openSocketConnection();
                if (connection.getAddress().getProxy() != null) {
                    makeTunnel(policy, connection, getRequestHeaders());
                }
            }

            // if super.makeConnection returned a connection from the
            // pool, sslSocket needs to be initialized here. If it is
            // a new connection, it will be initialized by
            // getSecureSocket below.
            sslSocket = connection.getSecureSocketIfConnected();

            // we already have an SSL connection,
            if (sslSocket != null) {
                return true;
            }

            connection.setupSecureSocket(enclosing.getSSLSocketFactory(), tlsTolerant);
            return false;
        }

        /**
         * To make an HTTPS connection over an HTTP proxy, send an unencrypted
         * CONNECT request to create the proxy connection. This may need to be
         * retried if the proxy requires authorization.
         *
         * @param policy the policy
         * @param connection the connection
         * @param requestHeaders the request headers
         * @throws IOException Signals that an I/O exception has occurred.
         */
        private void makeTunnel(HttpURLConnectionImpl policy, HttpConnection connection,
                RequestHeaders requestHeaders) throws IOException {
            RawHeaders rawRequestHeaders = requestHeaders.getHeaders();
            while (true) {
                HttpEngine connect = new ProxyConnectEngine(policy, rawRequestHeaders, connection);
                connect.sendRequest();
                connect.readResponse();

                int responseCode = connect.getResponseCode();
                switch (connect.getResponseCode()) {
                case HTTP_OK:
                    return;
                case HTTP_PROXY_AUTH:
                    rawRequestHeaders = new RawHeaders(rawRequestHeaders);
                    boolean credentialsFound = policy.processAuthHeader(HTTP_PROXY_AUTH,
                            connect.getResponseHeaders(), rawRequestHeaders);
                    if (credentialsFound) {
                        continue;
                    } else {
                        throw new IOException("Failed to authenticate with proxy");
                    }
                default:
                    throw new IOException("Unexpected response code for CONNECT: " + responseCode);
                }
            }
        }

        /**
         * Accept cache response type.
         *
         * @param cacheResponse the cache response
         * @return true, if successful
         * @see libcore.net.http.HttpEngine#acceptCacheResponseType(java.net.CacheResponse)
         */
        @Override protected boolean acceptCacheResponseType(CacheResponse cacheResponse) {
            return cacheResponse instanceof SecureCacheResponse;
        }

        /**
         * Include authority in request line.
         *
         * @return true, if successful
         * @see libcore.net.http.HttpEngine#includeAuthorityInRequestLine()
         */
        @Override protected boolean includeAuthorityInRequestLine() {
            // Even if there is a proxy, it isn't involved. Always request just the file.
            return false;
        }

        // Android's HttpsUrlConnection has a getSslSocketFactory method, while Java's does not.
        // So in Android, this is @Override, but here, it cannot be.
        /* (non-Javadoc)
         * @see libcore.net.http.HttpEngine#getSslSocketFactory()
         */
        protected SSLSocketFactory getSslSocketFactory() {
            return enclosing.getSSLSocketFactory();
        }

        /**
         * Gets the http connection to cache.
         *
         * @return the http connection to cache
         * @see libcore.net.http.HttpEngine#getHttpConnectionToCache()
         */
        @Override protected HttpURLConnection getHttpConnectionToCache() {
            return enclosing;
        }
    }

    /**
     * The Class ProxyConnectEngine.
     */
    private static class ProxyConnectEngine extends HttpEngine {
        
        /**
         * Instantiates a new proxy connect engine.
         *
         * @param policy the policy
         * @param requestHeaders the request headers
         * @param connection the connection
         * @throws IOException Signals that an I/O exception has occurred.
         */
        public ProxyConnectEngine(HttpURLConnectionImpl policy, RawHeaders requestHeaders,
                HttpConnection connection) throws IOException {
            super(policy, HttpEngine.CONNECT, requestHeaders, connection, null);
        }

        /**
         * If we're establishing an HTTPS tunnel with CONNECT (RFC 2817 5.2), send
         * only the minimum set of headers. This avoids sending potentially
         * sensitive data like HTTP cookies to the proxy unencrypted.
         *
         * @return the network request headers
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override protected RawHeaders getNetworkRequestHeaders() throws IOException {
            RequestHeaders privateHeaders = getRequestHeaders();
            URL url = policy.getURL();

            RawHeaders result = new RawHeaders();
            result.setStatusLine("CONNECT " + url.getHost() + ":" + URLs.getEffectivePort(url)
                    + " HTTP/1.1");

            // Always set Host and User-Agent.
            String host = privateHeaders.getHost();
            if (host == null) {
                host = getOriginAddress(url);
            }
            result.set("Host", host);

            String userAgent = privateHeaders.getUserAgent();
            if (userAgent == null) {
                userAgent = getDefaultUserAgent();
            }
            result.set("User-Agent", userAgent);

            // Copy over the Proxy-Authorization header if it exists.
            String proxyAuthorization = privateHeaders.getProxyAuthorization();
            if (proxyAuthorization != null) {
                result.set("Proxy-Authorization", proxyAuthorization);
            }

            // Always set the Proxy-Connection to Keep-Alive for the benefit of
            // HTTP/1.0 proxies like Squid.
            result.set("Proxy-Connection", "Keep-Alive");
            return result;
        }

        /**
         * Requires tunnel.
         *
         * @return true, if successful
         * @see libcore.net.http.HttpEngine#requiresTunnel()
         */
        @Override protected boolean requiresTunnel() {
            return true;
        }
    }
}
