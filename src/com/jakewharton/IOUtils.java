package com.jakewharton;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;


/** 
 * From libcore.io.IoUtils in Android 4.1.
 */
public class IOUtils {
    
    /**
     * Delete contents.
     *
     * @param dir the dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    static void deleteContents(File dir) throws IOException {
        File[] files = dir.listFiles();
        if (files == null) {
            throw new IllegalArgumentException("not a directory: " + dir);
        }
        for (File file : files) {
            if (file.isDirectory()) {
                deleteContents(file);
            }
            if (!file.delete()) {
                throw new IOException("failed to delete file: " + file);
            }
        }
    }

    /**
     * Close quietly.
     *
     * @param closeable the closeable
     */
    static void closeQuietly(/*Auto*/Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException rethrown) {
                throw rethrown;
            } catch (Exception ignored) {
            }
        }
    }
}
