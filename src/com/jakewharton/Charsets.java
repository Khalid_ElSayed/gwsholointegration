package com.jakewharton;

import java.nio.charset.Charset;


/** 
 * From java.nio.charset.Charsets, Android 4.1 
 * 
 */
class Charsets {
  
  /** The Constant US_ASCII. */
  static final Charset US_ASCII = Charset.forName("US-ASCII");
  
  /** The Constant UTF_8. */
  static final Charset UTF_8 = Charset.forName("UTF-8");
}
